package com.trackitil.fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.trackitil.BuildConfig;
import com.trackitil.ProjectUtils;
import com.trackitil.R;
import com.google.android.material.textfield.TextInputEditText;

public class LoginFragment extends Fragment {

    /**
     * A listener to the clickable component on the login fragment
     */
    public interface onButtonClickedListener {
        /**
         * Activates upon clicking the login button
         *
         * @param loginEmailField    - the email field
         * @param loginPasswordField - the password field
         * @param branchName         - the branch
         */
        void onLoginClicked(TextInputEditText loginEmailField, TextInputEditText loginPasswordField, String branchName);

        /**
         * Activates upon clicking the forget password button
         */
        void onForgotPasswordClicked();
    }

    /*
     * An instance of the listener
     */
    private onButtonClickedListener mListener;

    /* ==== fields and strings of the fragment ==== */
    private TextInputEditText mLoginEmailField;
    private TextInputEditText mLoginPasswordField;
    private String branchName;
    private Spinner spinner;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View inflatedView = inflater.inflate(R.layout.login_fragment, container, false);

        mLoginEmailField = inflatedView.findViewById(R.id.emailInput);
        mLoginPasswordField = inflatedView.findViewById(R.id.passwordInput);
        spinner = inflatedView.findViewById(R.id.branch_names);

        inflatedView.findViewById(R.id.loginButton).setOnClickListener(v -> {
            if (LoginFragment.this.mListener != null)
                LoginFragment.this.mListener.onLoginClicked(mLoginEmailField, mLoginPasswordField, branchName);
        });

        inflatedView.findViewById(R.id.forgot_password).setOnClickListener(v -> {
            if (LoginFragment.this.mListener != null)
                LoginFragment.this.mListener.onForgotPasswordClicked();
        });

        initSpinner();
        setSpinnerSelection();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                branchName = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                branchName = parent.getItemAtPosition(0).toString();
            }
        });

        return inflatedView;
    }

    /**
     * Initializes the spinner
     */
    private void initSpinner() {
        Context context = getContext();
        if (context == null)
            return;

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context,
                BuildConfig.DEBUG ? R.array.branches_debug : R.array.branches, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    /**
     * Sets the spinners selection to match the users branch
     */
    private void setSpinnerSelection() {
        String branch = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(ProjectUtils.BRANCH_KEY, "");
        if (branch != null && !branch.equals("")) {
            for (int i = 0; i < spinner.getAdapter().getCount(); i++) {
                if (spinner.getItemAtPosition(i).toString().equals(branch)) {
                    spinner.setSelection(i);
                    break;
                }
            }
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            onButtonClickedListener listener = (onButtonClickedListener) context;
            this.registerListener(listener);
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement onButtonClickedListener");
        }
    }

    private void registerListener(onButtonClickedListener listener) {
        this.mListener = listener;
    }
}