package com.trackitil.fragments;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ListView;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.trackitil.FirebaseHelper;
import com.trackitil.ProductAdapter;
import com.trackitil.ProjectUtils;
import com.trackitil.R;
import com.trackitil.logic.entities.ProductEntity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ShowProductsFragment extends Fragment implements ProductAdapter.RemoveListener, ProductAdapter.FilterDoneListener {

    /**
     * The list of products to show
     */
    private List<ProductEntity> products;

    /**
     * A filter instances to filter the search results
     */
    private SearchFilter searchFilter;

    /**
     * An instance of the product adapter
     */
    private ProductAdapter adapter;

    /**
     * a boolean that indicates if the filtering is over
     */
    private boolean done = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        ((Toolbar) container.findViewById(R.id.toolbar)).setTitle(R.string.show_inventory);
        View inflatedView = inflater.inflate(R.layout.show_products_fragment, container, false);

        this.searchFilter = new SearchFilter();
        this.products = new ArrayList<>();
        String branch = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(ProjectUtils.BRANCH_KEY, "");
        ListView listView = inflatedView.findViewById(R.id.products);
        adapter = new ProductAdapter(getContext(), products, this, this);
        listView.setAdapter(adapter);

        /* Querying over all the products, filtering them by branch*/
        Query allProductsQuery = FirebaseHelper.getInstance().getAllProducts();

        allProductsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot data : dataSnapshot.getChildren())
                        if (data.exists())
                            products.add(data.getValue(ProductEntity.class));
                    adapter.getFilter().filter(branch);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                //
            }
        });

        /* Applying a listener on the search view, to query the results by name*/
        ((SearchView) inflatedView.findViewById(R.id.search_view))
                .setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        if (done) {
                            if (newText.equals("")) {
                                products = adapter.getOrigin();
                                adapter.setToOrigin();
                            } else {
                                searchFilter.filter(newText);
                            }
                        }

                        return false;
                    }
                });

        return inflatedView;
    }

    @Override
    public void onRemove(int position) {
        String productToRemove = products.get(position).getId();
        FirebaseHelper.getInstance().removeProductById(productToRemove);
        products.remove(position);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onFilterDone() {
        products = adapter.getData();
        done = true;
    }

    /**
     * Utility class to filter search results by name
     */
    private class SearchFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String stringToFilter = constraint.toString();

            FilterResults results = new FilterResults();
            final List<ProductEntity> list = products;
            ArrayList<ProductEntity> newList = new ArrayList<>();

            for (ProductEntity productEntity : list) {
                if (productEntity.getName().contains(stringToFilter))
                    newList.add(productEntity);
            }

            results.values = newList;
            results.count = newList.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<ProductEntity> list = (ArrayList<ProductEntity>) results.values;
            products = list;
            adapter.setData(list);
        }
    }
}