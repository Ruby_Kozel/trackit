package com.trackitil.fragments;

import java.util.Calendar;

import com.trackitil.ProjectUtils;
import com.trackitil.R;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

public class AlarmPickerFragment extends Fragment {
    private TextView time_chosen;
    private TimePickerDialog.OnTimeSetListener mTimeSetListener;
    private SharedPreferences sharedPreferences;

    public interface onTimeListener {

        /**
         * Activates when a time gets picked
         *
         * @param hour   - the hour the user picked
         * @param minute - the minutes the user picked
         */

        void onTimeClicked(String hour, String minute);
    }

    /**
     * The instance of the listener
     */
    private onTimeListener mListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View inflatedView = inflater.inflate(R.layout.alarm_picker_fragment, container, false);
        time_chosen = inflatedView.findViewById(R.id.time_chosen);

        showHourPicker();
        if(sharedPreferences != null) {
            String hour = sharedPreferences.getString("HOUR", "");
            String minute = sharedPreferences.getString("MINUTE", "");
            if(!hour.equals("") && !minute.equals(""))
                time_chosen.setText(getString(R.string.show_time, hour, minute));
            else
                time_chosen.setText(getString(R.string.choose_time));
        }
        ((Toolbar) container.findViewById(R.id.toolbar)).setTitle(R.string.pickTime);
        return inflatedView;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            AlarmPickerFragment.onTimeListener listener = (AlarmPickerFragment.onTimeListener) context;
            this.registerListener(listener);
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement onDateListener");
        }
    }

    private void registerListener(AlarmPickerFragment.onTimeListener listener) {
        this.mListener = listener;
    }

    private void showHourPicker() {
        time_chosen.setOnClickListener(view -> {
            Calendar myCalender = Calendar.getInstance();
            int hour = myCalender.get(Calendar.HOUR_OF_DAY);
            int minute = myCalender.get(Calendar.MINUTE);

            TimePickerDialog dialog = new TimePickerDialog(getActivity(),
                    android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    mTimeSetListener,
                    hour,
                    minute,
                    true);

            dialog.setTitle(R.string.chooseTime);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.show();
        });

        mTimeSetListener = (view, hourOfDay, minute) -> {
            time_chosen.setText(getString(R.string.show_time, (hourOfDay < 10 ? "0" : "")+ hourOfDay , (minute < 10 ? "0" : "") + minute));

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("HOUR", hourOfDay + "");
            editor.putString("MINUTE", minute + "");
            editor.apply();

            ProjectUtils.makeToast(getContext(), getString(R.string.time_saved));

            if (AlarmPickerFragment.this.mListener != null)
                AlarmPickerFragment.this.mListener.onTimeClicked(Integer.toString(hourOfDay), Integer.toString(minute));
        };
    }
}