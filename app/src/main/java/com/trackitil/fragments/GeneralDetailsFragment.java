package com.trackitil.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Highlight;
import com.trackitil.FirebaseHelper;
import com.trackitil.ProjectUtils;
import com.trackitil.R;
import com.trackitil.logic.entities.ProductEntity;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class GeneralDetailsFragment extends Fragment {

    public interface CardClickListener {
        void onInventoryCardClicked();
        void onExpiredCardClicked();
        void onPieChartDateClicked(String date);
        void onFloatingButtonClicked();
    }

    private CardClickListener cardClickListener;
    private String branch;
    private int loop = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View inflatedView = inflater.inflate(R.layout.general_details_fragment, container, false);

        String date = ProjectUtils.formatter.format(new Date());

        branch = PreferenceManager.getDefaultSharedPreferences(container.getContext()).getString(ProjectUtils.BRANCH_KEY, "");
        ((TextView)inflatedView.findViewById(R.id.branch_text)).setText(branch);

        setListenerForTotalInventory(inflatedView.findViewById(R.id.total_inventory));
        setListenerForTotalExpired(inflatedView.findViewById(R.id.total_expired), date);

        inflatedView.findViewById(R.id.click_to_move_to_inventory).setOnClickListener(v -> {
            if (GeneralDetailsFragment.this.cardClickListener != null)
                cardClickListener.onInventoryCardClicked();
        });

        inflatedView.findViewById(R.id.click_to_move_to_expired).setOnClickListener(v -> {
            if (GeneralDetailsFragment.this.cardClickListener != null)
                cardClickListener.onExpiredCardClicked();
        });

        inflatedView.findViewById(R.id.floatingActionButton).setOnClickListener(v -> {
            if (GeneralDetailsFragment.this.cardClickListener != null)
                cardClickListener.onFloatingButtonClicked();
        });

        setPieChart(inflatedView, date);

        return inflatedView;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            CardClickListener cardClickListener = (CardClickListener) context;
            this.registerListener(cardClickListener);
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement CardClickListener");
        }
    }

    private void registerListener(CardClickListener cardClickListener) {
        this.cardClickListener = cardClickListener;
    }

    private void setListenerForTotalInventory(TextView total_inventory) {
        FirebaseHelper.getInstance().getAllProducts().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int counter = getCount(dataSnapshot);
                total_inventory.setText(getString(R.string.total_inventory, counter + ""));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setListenerForTotalExpired(TextView total_expired, String date) {
        FirebaseHelper.getInstance().getExpirationEqualTo(date).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int counter = getCount(dataSnapshot);
                total_expired.setText(getString(R.string.total_expired, counter + ""));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void setPieChart(View inflatedView, String date) {
        ArrayList<Entry> productCounts = new ArrayList<>();
        ArrayList<String> dates = new ArrayList<>();
        loop = 0;
        for (int i = 0; i < 6; i++) {
            String nextDate = ProjectUtils.addToDate(date, i + 1);
            FirebaseHelper
                    .getInstance()
                    .getExpirationEqualTo(nextDate)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            int counter = getCount(dataSnapshot);
                            if (counter > 0) {
                                productCounts.add(new Entry(counter, loop));
                                dates.add(nextDate);
                            }
                            if (loop == 5) drawPieChart(productCounts, dates, inflatedView);
                            else loop++;
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
        }
    }

    private void drawPieChart(ArrayList<Entry> productCounts, ArrayList<String> dates, View inflatedView) {
        PieDataSet pieData = new PieDataSet(productCounts, "");
        pieData.setColors(ColorTemplate.PASTEL_COLORS);

        PieData data = new PieData(dates, pieData);
        data.setValueTextSize(15f);
        data.setValueTextColor(Color.BLACK);
        data.notifyDataChanged();
        PieChart pie_Chart = inflatedView.findViewById(R.id.pie_chart);
        pie_Chart.setData(data);
        pie_Chart.notifyDataSetChanged();
        pie_Chart.setDrawHoleEnabled(true);
        pie_Chart.setHoleRadius(10f);
        pie_Chart.setTransparentCircleRadius(20f);
        pie_Chart.animateXY(1500, 1500);
        pie_Chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                if (GeneralDetailsFragment.this.cardClickListener != null)
                    cardClickListener.onPieChartDateClicked(dates.get(h.getXIndex()));
            }

            @Override
            public void onNothingSelected() {

            }
        });
    }

    private int getCount(DataSnapshot dataSnapshot) {
        int counter = 0;
        if (!dataSnapshot.exists())
            return counter;
        for (DataSnapshot child : dataSnapshot.getChildren()) {
            ProductEntity productEntity = child.getValue(ProductEntity.class);
            if (productEntity.getBranchName().equals(branch)) {
                counter++;
            }
        }
        return counter;
    }
}