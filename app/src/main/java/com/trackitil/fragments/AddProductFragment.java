package com.trackitil.fragments;

import android.content.Context;
import android.hardware.camera2.CameraAccessException;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.trackitil.R;

public class AddProductFragment extends Fragment {

    /**
     * A listener to listen to barcode scanning functionality
     */
    public interface BarcodeScanListener {
        /**
         * Activates when the scan button is clicked
         *
         * @throws CameraAccessException - if for any reason there's a problem with the camera of the device
         */
        void onBarcodeScanner() throws CameraAccessException;

        /**
         * Activates when the user manually adds a barcode
         */
        void onAddBarcode();
    }

    private BarcodeScanListener mListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View inflatedView = inflater.inflate(R.layout.add_products_fragment, container, false);

        inflatedView.findViewById(R.id.scan_button).setOnClickListener(v -> {
            if (AddProductFragment.this.mListener != null)
                try {
                    AddProductFragment.this.mListener.onBarcodeScanner();
                } catch (CameraAccessException e) {
                    throw new RuntimeException(e);
                }

        });

        inflatedView.findViewById(R.id.add_barcode).setOnClickListener(v -> {
            if (AddProductFragment.this.mListener != null)
                AddProductFragment.this.mListener.onAddBarcode();
        });

        ((Toolbar) container.findViewById(R.id.toolbar)).setTitle(R.string.add_products);

        return inflatedView;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            BarcodeScanListener listener = (BarcodeScanListener) context;
            this.registerListener(listener);
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement BarcodeScanListener");
        }
    }

    private void registerListener(BarcodeScanListener listener) {
        this.mListener = listener;
    }
}