package com.trackitil.fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;

import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    /**
     * A listener to the date pick
     */
    public interface OnDatePickedListener {
        /**
         * Activates when the user picks a date
         *
         * @param day   - the day the user picked
         * @param month - the month the user picked
         * @param year  - the year the user picked
         */
        void setDate(int day, int month, int year);
    }

    /**
     * Instance of the listener
     */
    private OnDatePickedListener mListener;

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        return new DatePickerDialog(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_DARK, this, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
    }

    public void registerListener(OnDatePickedListener mListener) {
        if (this.mListener == null)
            this.mListener = mListener;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        mListener.setDate(day, month + 1, year);
    }
}