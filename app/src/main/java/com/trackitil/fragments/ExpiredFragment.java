package com.trackitil.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.Toolbar;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.trackitil.FirebaseHelper;
import com.trackitil.ProjectUtils;
import com.trackitil.ProductAdapter;
import com.trackitil.R;
import com.trackitil.logic.entities.ProductEntity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ExpiredFragment extends Fragment implements ProductAdapter.RemoveListener, ProductAdapter.FilterDoneListener {
    /**
     * The product list to show
     */
    private List<ProductEntity> products;

    /**
     * Text view to show if no products are expired
     */
    private TextView noProductsTextView;

    /**
     * An instance of the product adapter
     */
    private ProductAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View inflatedView = inflater.inflate(R.layout.expired_fragment, container, false);

        String date = getArguments().getString(ProjectUtils.DATE_KEY);
        ListView listView = inflatedView.findViewById(R.id.expired_list);

        noProductsTextView = inflatedView.findViewById(R.id.no_products);
        products = new ArrayList<>();
        adapter = new ProductAdapter(getContext(), products, this, this);

        listView.setAdapter(adapter);
        String branch = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(ProjectUtils.BRANCH_KEY, "");

        if (adapter.getCount() == 0)
            noProductsTextView.setVisibility(View.VISIBLE);

        /* Querying over the expired products and filtering by branch */
        FirebaseHelper.getInstance().getExpirationEqualTo(date).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists())
                    for (DataSnapshot data : dataSnapshot.getChildren())
                        if (data.exists())
                            products.add(data.getValue(ProductEntity.class));
                    adapter.getFilter().filter(branch);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                //
            }
        });

        ((Toolbar) container.findViewById(R.id.toolbar)).setTitle(getString(R.string.expired_for, date));

        return inflatedView;
    }

    @Override
    public void onRemove(int position) {
        String productToRemove = adapter.getData().get(position).getId();
        FirebaseHelper.getInstance().removeProductById(productToRemove);
        products.remove(position);
        adapter.notifyDataSetChanged();

        if (adapter.getCount() == 0)
            noProductsTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFilterDone() {
        products = adapter.getData();

        if (adapter.getCount() != 0) {
            if (noProductsTextView.getVisibility() == View.VISIBLE)
                noProductsTextView.setVisibility(View.GONE);
        } else {
            noProductsTextView.setVisibility(View.VISIBLE);
        }
    }
}