package com.trackitil.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.trackitil.R;
import com.google.android.material.textfield.TextInputEditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class ResetPasswordFragment extends Fragment {

    /**
     * A listener to the reset button
     */
    public interface ResetButtonListener {
        /**
         * Activates when the user clicks on the reset button
         *
         * @param emailField - the email field in the fragment
         */
        void onResetClicked(TextInputEditText emailField);
    }

    /**
     * Instance of the listener
     */
    private ResetButtonListener mListener;

    /**
     * The email field to be sending the reset password instructions
     */
    private TextInputEditText mLoginEmailField;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View inflatedView = inflater.inflate(R.layout.reset_password_fragment, container, false);

        mLoginEmailField = inflatedView.findViewById(R.id.emailInput);

        inflatedView.findViewById(R.id.passwordButton).setOnClickListener(v -> {
            if (ResetPasswordFragment.this.mListener != null)
                ResetPasswordFragment.this.mListener.onResetClicked(mLoginEmailField);
        });

        return inflatedView;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            ResetPasswordFragment.ResetButtonListener listener = (ResetPasswordFragment.ResetButtonListener) context;
            this.registerListener(listener);
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement ResetButtonListener");
        }
    }

    private void registerListener(ResetPasswordFragment.ResetButtonListener listener) {
        this.mListener = listener;
    }
}