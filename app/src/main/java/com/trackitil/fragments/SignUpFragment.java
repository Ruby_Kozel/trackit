package com.trackitil.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.trackitil.BuildConfig;
import com.trackitil.ProjectUtils;
import com.trackitil.R;
import com.google.android.material.textfield.TextInputEditText;
import com.trackitil.logic.boundries.NewUserForm;

public class SignUpFragment extends Fragment {

    /**
     * A listener to the submit button
     */
    public interface onSubmitButtonListener {

        /**
         * Activates when a user clicks on submit
         * @param userForm - the user form
         */
        void onSubmitClicked(NewUserForm userForm);
    }

    /**
     * The instance of the listener
     */
    private onSubmitButtonListener mListener;

    /*  ==== The field texts and strings ====*/
    private TextInputEditText mSignUpEmailField;
    private TextInputEditText mSignUpPasswordField;
    private TextInputEditText mSignUpUserNameField;
    private String branchName;
    private String role;

    /**
     * An instance of spinner to present a combo box
     */
    private Spinner spinner;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View inflatedView = inflater.inflate(R.layout.signup_fragment, container, false);

        mSignUpEmailField = inflatedView.findViewById(R.id.emailInputSignUp);
        mSignUpPasswordField = inflatedView.findViewById(R.id.passwordInputSignUp);
        mSignUpUserNameField = inflatedView.findViewById(R.id.userName);
        spinner = inflatedView.findViewById(R.id.branch_names);

        inflatedView.findViewById(R.id.signUpButton).setOnClickListener(v -> {
            NewUserForm userForm = new NewUserForm(mSignUpUserNameField, mSignUpEmailField, mSignUpPasswordField, branchName, role);
            if (SignUpFragment.this.mListener != null)
                SignUpFragment.this.mListener.onSubmitClicked(userForm);
        });

        initSpinner();
        setSpinnerSelection();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                branchName = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                branchName = parent.getItemAtPosition(0).toString();
            }
        });

        ((RadioGroup) inflatedView.findViewById(R.id.role_group)).check(R.id.worker);
        role = ProjectUtils.WORKER;
        ((RadioGroup) inflatedView.findViewById(R.id.role_group)).setOnCheckedChangeListener((radioGroup, checkedId) ->
                        role = checkedId == R.id.manager ? ProjectUtils.MANAGER : ProjectUtils.WORKER);

        ((Toolbar) container.findViewById(R.id.toolbar)).setTitle(R.string.addUser);
        return inflatedView;
    }

    /**
     * Initializes the spinner
     */
    private void initSpinner() {
        Context context = getContext();
        if (context == null)
            return;

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context,
                BuildConfig.DEBUG ? R.array.branches_debug : R.array.branches, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    /**
     * Sets the spinners selection to match the users branch
     */
    private void setSpinnerSelection() {
        String branch = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(ProjectUtils.BRANCH_KEY, "");
        if (branch != null && !branch.equals("")) {
            for (int i = 0; i < spinner.getAdapter().getCount(); i++) {
                if (spinner.getItemAtPosition(i).toString().equals(branch)) {
                    spinner.setSelection(i);
                    break;
                }
            }
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            onSubmitButtonListener listener = (onSubmitButtonListener) context;
            this.registerListener(listener);
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement onSubmitButtonListener");
        }
    }

    private void registerListener(onSubmitButtonListener listener) {
        this.mListener = listener;
    }
}