package com.trackitil.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.textfield.TextInputEditText;
import com.trackitil.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

public class RemoveUserFragment extends Fragment {

    public interface UserRemoveListener {
        void onUserRemove(TextInputEditText emailField);
    }

    private TextInputEditText emailField;

    private UserRemoveListener mListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View inflatedView = inflater.inflate(R.layout.remove_user_fragment, container, false);
        emailField = inflatedView.findViewById(R.id.emailInput);

        inflatedView.findViewById(R.id.removeButton).setOnClickListener(v -> {
            if(RemoveUserFragment.this.mListener != null)
                RemoveUserFragment.this.mListener.onUserRemove(emailField);
        });
        ((Toolbar) container.findViewById(R.id.toolbar)).setTitle(R.string.removeUser);
        return inflatedView;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            RemoveUserFragment.UserRemoveListener listener = (RemoveUserFragment.UserRemoveListener) context;
            this.registerListener(listener);
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement UserRemoveListener");
        }
    }

    private void registerListener(RemoveUserFragment.UserRemoveListener listener) {
        this.mListener = listener;
    }
}
