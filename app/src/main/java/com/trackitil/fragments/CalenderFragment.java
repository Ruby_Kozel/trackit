package com.trackitil.fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.Toolbar;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.content.Context;
import android.os.Bundle;

import com.trackitil.ProjectUtils;
import com.trackitil.R;

public class CalenderFragment extends Fragment {

    /**
     * A listener to date clicks
     */
    public interface onDateListener {
        /**
         * Activates when a date gets picked
         *
         * @param date - the date the user picked
         */
        void onDateClicked(String date);
    }

    /**
     * The instance of the listener
     */
    private onDateListener mListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View inflatedView = inflater.inflate(R.layout.calender_fragment, container, false);

        CalendarView calendarView = inflatedView.findViewById(R.id.calendarView);

        calendarView.setOnDateChangeListener((view, year, month, dayOfMonth) -> {
            if (CalenderFragment.this.mListener != null)
                CalenderFragment.this.mListener.onDateClicked(ProjectUtils.getDate(dayOfMonth, month + 1, year));
        });

        ((Toolbar) container.findViewById(R.id.toolbar)).setTitle(R.string.calender);
        return inflatedView;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            onDateListener listener = (onDateListener) context;
            this.registerListener(listener);
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement onDateListener");
        }
    }

    private void registerListener(onDateListener listener) {
        this.mListener = listener;
    }
}