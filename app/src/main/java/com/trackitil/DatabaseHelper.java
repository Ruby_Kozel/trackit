package com.trackitil;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.trackitil.logic.boundries.NewProductForm;
import com.trackitil.logic.boundries.NewUserForm;

public interface DatabaseHelper {
    void addBarcodeMapEntity(String barcode, String name);
    void addProduct(NewProductForm productForm);
    void addUser(NewUserForm userForm);
    Query getAllProducts();
    Query getUser(String email);
    void removeProductById(String id);
    DatabaseReference getProductById(String id);
    Query getExpirationEqualTo(String date);
    Query queryForShortBarcode(String barcode);
    Query queryForLongBarcode(String barcode);
}