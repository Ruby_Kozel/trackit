package com.trackitil.logic.boundries;

import com.google.android.material.textfield.TextInputEditText;

import java.util.List;

public interface IForm {

    List<TextInputEditText> getEditors();
}
