package com.trackitil.logic.boundries;

import com.google.android.material.textfield.TextInputEditText;

import java.util.Arrays;
import java.util.List;

public class NewUserForm implements IForm {

    private TextInputEditText userName;
    private TextInputEditText email;
    private TextInputEditText password;
    private String branch;
    private String role;

    public NewUserForm(TextInputEditText userName, TextInputEditText email, TextInputEditText password, String branch, String role) {
        this.branch = branch;
        this.email = email;
        this.password = password;
        this.role = role;
        this.userName = userName;
    }

    public TextInputEditText getUserName() {
        return userName;
    }

    public String getUserNameValue() {
        return userName.getText().toString();
    }

    public void setUserName(TextInputEditText userName) {
        this.userName = userName;
    }

    public TextInputEditText getEmail() {
        return email;
    }

    public String getEmailValue() {
        return email.getText().toString();
    }

    public void setEmail(TextInputEditText email) {
        this.email = email;
    }

    public TextInputEditText getPassword() {
        return password;
    }

    public String getPasswordValue() {
        return password.getText().toString();
    }

    public void setPassword(TextInputEditText password) {
        this.password = password;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getRole() { return role; }

    public void setRole(String role) { this.role = role; }

    @Override
    public List<TextInputEditText> getEditors() { return Arrays.asList(userName, password, email); }

    @Override
    public String toString() {
        return "NewUserForm{" +
                "userName=" + getUserNameValue() +
                ", email=" + getEmailValue() +
                ", password=" + getPasswordValue() +
                ", branch='" + getBranch() + '\'' +
                ", role='" + getRole() + '\'' +
                '}';
    }
}
