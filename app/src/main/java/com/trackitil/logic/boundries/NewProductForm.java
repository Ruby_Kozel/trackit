package com.trackitil.logic.boundries;

import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.TextInputEditText;
import com.trackitil.ProjectUtils;

import java.util.Arrays;
import java.util.List;

public class NewProductForm implements IForm {

    private TextInputEditText barcodeField;
    private TextInputEditText nameField;
    private TextInputEditText dateField;
    private TextInputEditText packagesField;
    private TextInputEditText quantityField;
    private MaterialCheckBox onShelfField;
    private String branch;

    public NewProductForm(TextInputEditText barcodeField,
                          TextInputEditText nameField,
                          TextInputEditText dateField,
                          TextInputEditText packagesField,
                          TextInputEditText quantityField,
                          MaterialCheckBox onShelfField,
                          String branch) {
        this.barcodeField = barcodeField;
        this.nameField = nameField;
        this.dateField = dateField;
        this.packagesField = packagesField;
        this.quantityField = quantityField;
        this.onShelfField = onShelfField;
        this.branch = branch;
    }

    public TextInputEditText getBarcodeField() {
        return barcodeField;
    }

    public String getBarcodeValue() {
        return barcodeField.getText().toString();
    }

    public void setBarcodeField(TextInputEditText barcodeField) {
        this.barcodeField = barcodeField;
    }

    public TextInputEditText getNameField() {
        return nameField;
    }

    public String getNameValue() {
        return nameField.getText().toString();
    }

    public void setNameField(TextInputEditText nameField) {
        this.nameField = nameField;
    }

    public TextInputEditText getDateField() {
        return dateField;
    }

    public String getDateValueString() {
        return dateField.getText().toString();
    }

    public String getDateValueFormatted() {
        return ProjectUtils.getDate(getDateValueString());
    }

    public void setDateField(TextInputEditText dateField) {
        this.dateField = dateField;
    }

    public TextInputEditText getPackagesField() {
        return packagesField;
    }

    public String getPackagesValue() {
        return packagesField.getText().toString();
    }

    public void setPackagesField(TextInputEditText packagesField) {
        this.packagesField = packagesField;
    }

    public TextInputEditText getQuantityField() {
        return quantityField;
    }

    public String getQuantityValue() {
        return quantityField.getText().toString();
    }

    public void setQuantityField(TextInputEditText quantityField) {
        this.quantityField = quantityField;
    }

    public MaterialCheckBox getOnShelfField() {
        return onShelfField;
    }

    public boolean getOnShelfValue() {
        return onShelfField.isChecked();
    }

    public void setOnShelfField(MaterialCheckBox onShelfField) {
        this.onShelfField = onShelfField;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    @Override
    public List<TextInputEditText> getEditors() {
        return Arrays.asList(barcodeField, nameField, dateField, packagesField, quantityField);
    }
}
