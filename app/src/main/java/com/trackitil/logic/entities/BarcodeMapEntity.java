package com.trackitil.logic.entities;

import com.google.firebase.database.IgnoreExtraProperties;

import org.jetbrains.annotations.NotNull;

@IgnoreExtraProperties
@SuppressWarnings("unused")
public class BarcodeMapEntity {
    private String barcode;
    private String name;

    public BarcodeMapEntity() {
    }

    /**
     * A logic class of a barcode map to map barcodes to names and store in the database
     *
     * @param barcode - the barcode to map
     * @param name    - the mapped name
     */
    public BarcodeMapEntity(String barcode, String name) {
        this.barcode = barcode;
        this.name = name;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    @NotNull
    public String toString() {
        return "BarcodeMapEntity{" +
                "barcode='" + barcode + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}