package com.trackitil.logic.entities;

import com.google.firebase.database.IgnoreExtraProperties;
import com.trackitil.logic.boundries.NewUserForm;

import androidx.annotation.NonNull;

@IgnoreExtraProperties
@SuppressWarnings("unused")
public class UserEntity {

    private String name;
    private String email;
    private String role;
    private String branchName;

    public UserEntity() {
    }

    public UserEntity(NewUserForm userForm) {
        this(userForm.getUserNameValue(), userForm.getEmailValue(), userForm.getBranch(), userForm.getRole());
    }

    /**
     * A logic class of a user to be used to store the users in the database
     *
     * @param name       - the name of the user
     * @param email      - the email of the user
     * @param branchName - the branch this user belongs to
     * @param role       - the role of the user
     */
    private UserEntity(String name, String email, String branchName, String role) {
        this.name = name;
        this.email = email;
        this.role = role;
        this.branchName = branchName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    @Override
    @NonNull
    public String toString() {
        return "UserEntity{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", role='" + role + '\'' +
                ", branchName='" + branchName + '\'' +
                '}';
    }
}