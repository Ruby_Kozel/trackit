package com.trackitil.logic.entities;

import com.trackitil.ProjectUtils;
import com.google.firebase.database.IgnoreExtraProperties;
import com.trackitil.logic.boundries.NewProductForm;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

@IgnoreExtraProperties
@SuppressWarnings("unused")
public class ProductEntity {

    private String id;
    private String name;
    private long numberOfPackages;
    private long productsInPackage;
    private boolean onShelf;
    private String expirationDate;
    private String creationDate;
    private String branchName;

    public ProductEntity() {

    }

    public ProductEntity(String id, NewProductForm productForm) {
        this(id, productForm.getNameValue(),
                Long.parseLong(productForm.getPackagesValue()),
                Long.parseLong(productForm.getQuantityValue()),
                productForm.getOnShelfValue(),
                productForm.getDateValueFormatted(),
                productForm.getBranch()
        );
    }

    /**
     * A logic class of a product to be used to store the products in the database
     *
     * @param id                - the unique identifier of the product
     * @param name              - the name of the product
     * @param numberOfPackages  - the number of packages of this product
     * @param productsInPackage - the number of product in a single package
     * @param onShelf           - a boolean that indicates if the product is on the shelf or not
     * @param expirationDate    - the expiration date of the product
     * @param branchName        - the branch this product belongs to
     */
    public ProductEntity(String id, String name, long numberOfPackages, long productsInPackage, boolean onShelf, String expirationDate, String branchName) {
        this.id = id;
        this.name = name;
        this.numberOfPackages = numberOfPackages;
        this.productsInPackage = productsInPackage;
        this.onShelf = onShelf;
        this.expirationDate = expirationDate;
        this.branchName = branchName;
        this.creationDate = ProjectUtils.formatter.format(new Date());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public long getNumberOfPackages() {
        return numberOfPackages;
    }

    public void setNumberOfPackages(long numberOfPackages) {
        this.numberOfPackages = numberOfPackages;
    }

    public long getProductsInPackage() {
        return productsInPackage;
    }

    public void setProductsInPackage(long productsInPackage) {
        this.productsInPackage = productsInPackage;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public void setOnShelf(boolean onShelf) {
        this.onShelf = onShelf;
    }

    public boolean isOnShelf() {
        return onShelf;
    }

    @Override
    @NotNull
    public String toString() {
        return "ProductEntity{" +
                "branchName=" + branchName +
                "name='" + name + '\'' +
                ", numberOfPackages=" + numberOfPackages +
                ", productsInPackage=" + productsInPackage +
                ", onShelf=" + onShelf +
                ", creationDate=" + creationDate +
                ", expirationDate=" + expirationDate +
                '}';
    }
}