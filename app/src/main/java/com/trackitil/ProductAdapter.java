package com.trackitil;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.trackitil.logic.entities.ProductEntity;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ProductAdapter extends BaseAdapter implements Filterable {

    /**
     * Listener to the Remove Button
     */
    public interface RemoveListener {
        /**
         * Activates when the user clicks on the remove button
         *
         * @param position - the position of the product the user clicked
         */
        void onRemove(int position);
    }

    /**
     * Listener to the filter
     */
    public interface FilterDoneListener {
        /**
         * Activates when the filter is finished it's work
         */
        void onFilterDone();
    }

    /**
     * The context of the application
     */
    private Context context;

    /**
     * The list that contains the actual products to be displayed
     */
    private List<ProductEntity> productEntities;

    /**
     * The entire list of products, filtered by the branch they are in
     */
    private List<ProductEntity> filteredByBranch;

    /**
     * The branch filter, which responsible for filtering the products by the branch specified
     */
    private BranchFilter filter;

    /**
     * Instances of the listeners noted above
     **/
    private RemoveListener removeListener;
    private FilterDoneListener filterDoneListener;

    /**
     * Adapter class to transfer the ProductEntity logic into a visual product
     *
     * @param context            - the context of the application
     * @param productEntities    - the list of products
     * @param removeListener     - the removeListener to be assigned
     * @param filterDoneListener - the filterDoneListener to be assigned
     */
    public ProductAdapter(Context context, List<ProductEntity> productEntities, RemoveListener removeListener, FilterDoneListener filterDoneListener) {
        this.context = context;
        this.productEntities = productEntities;
        this.filteredByBranch = new ArrayList<>();
        this.removeListener = removeListener;
        this.filterDoneListener = filterDoneListener;
        this.filter = new BranchFilter();
    }

    @Override
    public int getCount() {
        return productEntities.size();
    }

    @Override
    public Object getItem(int position) {
        return productEntities.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    public List<ProductEntity> getData() {
        return productEntities;
    }

    public List<ProductEntity> getOrigin() {
        return filteredByBranch;
    }

    public void setData(List<ProductEntity> productEntities) {
        this.productEntities = productEntities;
        notifyDataSetChanged();
    }

    /**
     * Sets the list of products to the original list, filtered by the branch
     */
    public void setToOrigin() {
        productEntities = filteredByBranch;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = LayoutInflater.from(context).inflate(R.layout.product_row, parent, false);

        /* ====== Product form components ====== */
        LinearLayout productInfo = convertView.findViewById(R.id.product_info);
        TextView productName = convertView.findViewById(R.id.product_name);
        TextView creationDate = convertView.findViewById(R.id.product_creation_date);
        TextView expirationDate = convertView.findViewById(R.id.product_expiration_date);
        TextView total = convertView.findViewById(R.id.total);
        MaterialCheckBox checkBox = convertView.findViewById(R.id.on_shelf);
        MaterialButton removeButton = convertView.findViewById(R.id.remove);

        ProductEntity productEntity = (ProductEntity) getItem(position);

        productName.setText(productEntity.getName());
        creationDate.setText(parent.getContext().getString(R.string.creation_date, productEntity.getCreationDate()));
        expirationDate.setText(parent.getContext().getString(R.string.expiration_date, productEntity.getExpirationDate()));
        total.setText(parent.getContext().getString(R.string.total, (productEntity.getNumberOfPackages() * productEntity.getProductsInPackage() + "")));
        checkBox.setChecked(productEntity.isOnShelf());

        productName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_arrow_drop_down_24px, 0, 0, 0);

        checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            productEntity.setOnShelf(isChecked);
            FirebaseHelper.getInstance().getProductById(productEntity.getId()).child("onShelf").setValue(isChecked);
        });

        productName.setOnClickListener(v -> {
            productInfo.setVisibility(productInfo.getVisibility() == GONE ? VISIBLE : GONE);
            productName.setCompoundDrawablesWithIntrinsicBounds(productInfo.getVisibility() == GONE ?
                    R.drawable.ic_baseline_arrow_drop_down_24px : R.drawable.ic_baseline_arrow_drop_up_24px, 0, 0, 0);
        });

        removeButton.setOnClickListener(v -> promptDeleteProduct(parent.getContext(), position, checkBox));
        return convertView;
    }

    /**
     * A prompt to be displayed to the user when clicking the remove button
     *
     * @param context  - the context of the application
     * @param position - the position of the product the user clicked on
     */
    private void promptDeleteProduct(Context context, int position, MaterialCheckBox checkBox) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.sure_to_delete);
        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
            checkBox.setOnCheckedChangeListener(null);
            ProductAdapter.this.removeListener.onRemove(position);
            ProjectUtils.makeToast(context, context.getString(R.string.product_deleted));
        });
        builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.dismiss());
        builder.show();
    }

    /**
     * Utility class to filter results by branch.
     */
    private class BranchFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String stringToFilter = constraint.toString();

            FilterResults results = new FilterResults();
            final List<ProductEntity> list = productEntities;
            ArrayList<ProductEntity> newList = new ArrayList<>();

            for (ProductEntity productEntity : list) {
                if (productEntity.getBranchName().equals(stringToFilter))
                    newList.add(productEntity);
            }

            results.values = newList;
            results.count = newList.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.values == null)
                return;
            filteredByBranch = (ArrayList<ProductEntity>) results.values;
            productEntities = filteredByBranch;
            notifyDataSetChanged();
            if (filterDoneListener != null)
                filterDoneListener.onFilterDone();
        }
    }
}