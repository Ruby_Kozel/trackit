package com.trackitil;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.preference.PreferenceManager;

import com.trackitil.activities.AuthorizedUserActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.trackitil.logic.entities.ProductEntity;

import java.util.Date;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

/**
 * A BroadcastReceiver class that sends a notification to the users
 */
public class AlarmReceiver extends BroadcastReceiver {

    private SharedPreferences sharedPreferences;

    @Override
    public void onReceive(Context context, Intent intent) {
        String branch = PreferenceManager.getDefaultSharedPreferences(context).getString(ProjectUtils.BRANCH_KEY, "");
        if (branch == null || branch.equals(""))
            return;

        FirebaseHelper
                .getInstance()
                .getExpirationEqualTo(ProjectUtils.formatter.format(new Date()))
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            if (dataSnapshot.getChildrenCount() > 0) {
                                int count = 0;
                                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                                    ProductEntity productEntity = ds.getValue(ProductEntity.class);
                                    if (productEntity != null && productEntity.getBranchName().equals(branch))
                                        count++;
                                }

                                if (count > 0)
                                    makeNotification(count, context);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

    }

    private void makeNotification(int count, Context context) {
        Intent notificationIntent = new Intent(context, AuthorizedUserActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(context, AuthorizedUserActivity.CHANNEL_ID)
                .setSmallIcon(R.drawable.trackit)
                .setContentTitle(context.getString(R.string.expired_notifications))
                .setContentText(context.getString(R.string.expired_notifications_alert, count + ""))
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT));

        ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE))
                .notify((int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE), mNotifyBuilder.build());
    }
}