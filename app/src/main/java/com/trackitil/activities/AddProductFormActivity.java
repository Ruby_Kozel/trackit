package com.trackitil.activities;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.trackitil.FirebaseHelper;
import com.trackitil.ProjectUtils;
import com.trackitil.R;
import com.trackitil.fragments.DatePickerFragment;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.TextInputEditText;
import com.trackitil.logic.boundries.NewProductForm;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class AddProductFormActivity extends AppCompatActivity implements DatePickerFragment.OnDatePickedListener {

    TextInputEditText barcodeField;
    TextInputEditText nameField;
    TextInputEditText dateField;
    TextInputEditText packagesField;
    TextInputEditText quantityField;
    MaterialCheckBox onShelfField;
    MaterialButton datePickerButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product_form);

        String barcode = getIntent().getBundleExtra(ProjectUtils.BUNDLE_KEY).getString(ProjectUtils.BARCODE_KEY);
        String product_name = getIntent().getBundleExtra(ProjectUtils.BUNDLE_KEY).getString(ProjectUtils.PRODUCT_NAME_KEY);
        String branch = PreferenceManager.getDefaultSharedPreferences(this).getString(ProjectUtils.BRANCH_KEY, "");

        barcodeField = findViewById(R.id.form_barcode_edit_text);
        nameField = findViewById(R.id.form_name_edit_text);
        dateField = findViewById(R.id.form_date_edit_text);
        packagesField = findViewById(R.id.form_number_of_packages_edit_text);
        quantityField = findViewById(R.id.form_quantity_in_package_edit_text);
        onShelfField = findViewById(R.id.form_on_shelf);
        datePickerButton = findViewById(R.id.date_picker_button);

        barcodeField.setText(barcode);
        nameField.setText(product_name);

        datePickerButton.setOnClickListener(v -> {
            DatePickerFragment datePickerFragment = new DatePickerFragment();
            datePickerFragment.registerListener(this);
            datePickerFragment.show(getSupportFragmentManager(), "datePicker");
        });

        findViewById(R.id.addProduct).setOnClickListener(v ->
                handleForm(new NewProductForm(barcodeField, nameField, dateField, packagesField, quantityField, onShelfField, branch)));
    }


    /**
     * Set Date
     */
    @Override
    public void setDate(int day, int month, int year) {
        dateField.setText(ProjectUtils.getDate(day, month, year));
    }

    /**
     * Handles the form upon clicking the add product button
     */
    private void handleForm(NewProductForm productForm) {
        if (ProjectUtils.validateForm(getString(R.string.field_error), productForm.getEditors())) {
            if (ProjectUtils.validateDate(productForm.getDateValueString())) {
                FirebaseHelper.getInstance().addProduct(productForm);
                ProjectUtils.clearFields(productForm.getEditors());
                ProjectUtils.makeToast(AddProductFormActivity.this, getString(R.string.product_added));
                finish();
            } else {
                dateField.setError(getString(R.string.format_error));
            }
        }
    }
}