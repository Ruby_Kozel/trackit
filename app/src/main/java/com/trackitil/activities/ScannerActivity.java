package com.trackitil.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.ImageReader;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import com.trackitil.FirebaseHelper;
import com.trackitil.ProjectUtils;
import com.trackitil.R;
import com.trackitil.logic.entities.BarcodeMapEntity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;

import java.util.Arrays;
import java.util.Collections;

/**
 * This activity is responsible to the take pictures, scan them and recognize barcodes in them
 */
public class ScannerActivity extends AppCompatActivity {

    /**
     * The view the camera screen will be displayed on
     */
    private TextureView textureView;

    /**
     * Orientation array and static appending to it
     */
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    /**
     * The instance of the camera device
     */
    private CameraDevice cameraDevice;

    /**
     * Instance of the capture session of the camera
     */
    private CameraCaptureSession cameraCaptureSessions;

    /**
     * Instance of the capture request builder
     **/

    private CaptureRequest.Builder captureRequestBuilder;

    /**
     * Camera screen size
     */
    private Size imageDimension;

    /**
     * The handler and the thread to handle the picture taking
     */
    private Handler mBackgroundHandler;
    private HandlerThread mBackgroundThread;

    /**
     * The permission code of the camera
     */
    private static final int REQUEST_CAMERA_PERMISSION = 200;

    /**
     * A texture listener
     */
    private TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
            openCamera();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scanner);

        textureView = findViewById(R.id.textureView);
        if (textureView != null)
            textureView.setSurfaceTextureListener(textureListener);

        findViewById(R.id.btnCapture).setOnClickListener(v -> takePicture());
        findViewById(R.id.finish).setOnClickListener(v -> finish());
    }

    /**
     * Takes a picture with the camera
     */
    private void takePicture() {
        if (cameraDevice == null)
            return;
        try {
            Size[] jpegSizes =
                    ((CameraManager) getSystemService(Context.CAMERA_SERVICE)).getCameraCharacteristics(cameraDevice.getId())
                            .get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)
                            .getOutputSizes(ImageFormat.YUV_420_888);

            //Capture image with custom size
            int width = 640;
            int height = 480;
            if (jpegSizes != null && jpegSizes.length > 0) {
                width = jpegSizes[0].getWidth();
                height = jpegSizes[0].getHeight();
            }

            final ImageReader reader = ImageReader.newInstance(width, height, ImageFormat.YUV_420_888, 1);
            final CaptureRequest.Builder captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(reader.getSurface());
            captureBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);

            //Check orientation base on device
            int rotation = getWindowManager().getDefaultDisplay().getRotation();
            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));

            reader.setOnImageAvailableListener(imageReader -> {
                //Try to recognize barcode
                FirebaseVisionImage image = FirebaseVisionImage.fromMediaImage(reader.acquireLatestImage(), rotation);
                FirebaseHelper.getInstance().detectBarcodesFrom(
                        // Image src
                        image,
                        // OnSuccessListener
                        barCodes -> {
                            String barcode;
                            try {
                                barcode = barCodes.get(0).getDisplayValue();
                            } catch (Exception e) {
                                notFoundBarcode();
                                return;
                            }

                            searchBarcodeInDatabase(barcode);
                        },
                        // OnFailureListener
                        e -> ProjectUtils.makeToast(ScannerActivity.this, getString(R.string.failed_recg_barcode)));
            }, mBackgroundHandler);

            // Creating the capture session
            cameraDevice.createCaptureSession(Arrays.asList(reader.getSurface(),
                    new Surface(textureView.getSurfaceTexture())),
                    new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                            try {
                                cameraCaptureSession.capture(captureBuilder.build(), new CameraCaptureSession.CaptureCallback() {
                                    @Override
                                    public void onCaptureCompleted(
                                            @NonNull CameraCaptureSession session,
                                            @NonNull CaptureRequest request,
                                            @NonNull TotalCaptureResult result) {
                                        super.onCaptureCompleted(session, request, result);
                                        createCameraPreview();
                                    }
                                }, mBackgroundHandler);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {

                        }
                    }, mBackgroundHandler);


        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a new camera screen preview
     */
    private void createCameraPreview() {
        try {
            SurfaceTexture texture = textureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(imageDimension.getWidth(), imageDimension.getHeight());
            Surface surface = new Surface(texture);
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(surface);
            cameraDevice.createCaptureSession(Collections.singletonList(surface), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    if (cameraDevice == null)
                        return;
                    cameraCaptureSessions = cameraCaptureSession;
                    updatePreview();
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                }
            }, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Updates the preview after taking a picture
     */
    private void updatePreview() {
        if (cameraDevice == null)
            return;
        captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CaptureRequest.CONTROL_MODE_AUTO);
        try {
            cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), null, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Open the devices camera
     */
    private void openCamera() {
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            String cameraId = manager.getCameraIdList()[0];
            StreamConfigurationMap map = manager.getCameraCharacteristics(cameraId).get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            assert map != null;
            imageDimension = map.getOutputSizes(SurfaceTexture.class)[0];
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.CAMERA,
                }, REQUEST_CAMERA_PERMISSION);
                return;
            }
            manager.openCamera(cameraId, new CameraDevice.StateCallback() {
                @Override
                public void onOpened(@NonNull CameraDevice camera) {
                    cameraDevice = camera;
                    createCameraPreview();
                }

                @Override
                public void onDisconnected(@NonNull CameraDevice cameraDevice) {
                    cameraDevice.close();
                }

                @Override
                public void onError(@NonNull CameraDevice cameraDevice, int i) {
                    cameraDevice.close();
                }
            }, null);

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                ProjectUtils.makeToast(ScannerActivity.this, getString(R.string.cant_use_camera));
                finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startBackgroundThread();
        if (textureView.isAvailable())
            openCamera();
        else
            textureView.setSurfaceTextureListener(textureListener);
    }

    @Override
    protected void onPause() {
        stopBackgroundThread();
        super.onPause();
    }

    private void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("Camera Background");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    /**
     * Shows a dialog when a barcode wasn't found
     */
    public void notFoundBarcode() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.no_barcode);
        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss());
        builder.setCancelable(false);
        builder.show();
    }

    /**
     * Shows a dialog when the barcode recognition succeeded, but the barcode mapper wasn't in the database
     */
    public void barcodeNotInDatabase(String barcode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.no_barcode_in_database);

        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
            Bundle b = new Bundle();
            b.putString(ProjectUtils.BARCODE_KEY, barcode);
            Intent intent = new Intent(ScannerActivity.this, AddBarcodeMapperFormActivity.class);
            intent.putExtra(ProjectUtils.BUNDLE_KEY, b);
            startActivity(intent);
        });

        builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.dismiss());

        builder.show();
    }

    /**
     * Moves the user to the add product form
     *
     * @param bme - the barcode mapper entity to take the barcode and name from
     */
    private void goToAddProductForm(BarcodeMapEntity bme) {
        Bundle b = new Bundle();
        b.putString(ProjectUtils.BARCODE_KEY, bme.getBarcode());
        b.putString(ProjectUtils.PRODUCT_NAME_KEY, bme.getName());
        Intent intent = new Intent(ScannerActivity.this, AddProductFormActivity.class);
        intent.putExtra(ProjectUtils.BUNDLE_KEY, b);
        startActivity(intent);
    }

    public void searchBarcodeInDatabase(String barcode) {
        // Query for product with the barcode above - short barcode
        Query shortBarcodeQuery = FirebaseHelper.getInstance().queryForShortBarcode(barcode);
        shortBarcodeQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // It is safe to assume that there's only one barcode, since the mapping is one-to-one
                if (dataSnapshot.exists()) {
                    BarcodeMapEntity bme = dataSnapshot.getChildren().iterator().next().getValue(BarcodeMapEntity.class);
                    if (bme == null)
                        return;
                    goToAddProductForm(bme);
                    shortBarcodeQuery.removeEventListener(this);
                } else {
                    // Query for product with the barcode above - long barcode
                    Query longBarcodeQuery = FirebaseHelper.getInstance().queryForLongBarcode(barcode);
                    longBarcodeQuery.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            // It is safe to assume that there's only one barcode, since the mapping is one-to-one
                            if (dataSnapshot.exists()) {
                                BarcodeMapEntity bme = dataSnapshot.getChildren().iterator().next().getValue(BarcodeMapEntity.class);
                                if (bme == null)
                                    return;
                                goToAddProductForm(bme);
                                longBarcodeQuery.removeEventListener(this);
                            } else {
                                barcodeNotInDatabase(barcode);
                                shortBarcodeQuery.removeEventListener(this);
                                longBarcodeQuery.removeEventListener(this);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }
}