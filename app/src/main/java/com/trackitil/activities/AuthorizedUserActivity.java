package com.trackitil.activities;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.trackitil.AlarmReceiver;
import com.trackitil.FirebaseHelper;
import com.trackitil.ProjectUtils;
import com.trackitil.R;
import com.trackitil.fragments.AddProductFragment;
import com.trackitil.fragments.AlarmPickerFragment;
import com.trackitil.fragments.CalenderFragment;
import com.trackitil.fragments.ExpiredFragment;
import com.trackitil.fragments.GeneralDetailsFragment;
import com.trackitil.fragments.RemoveUserFragment;
import com.trackitil.fragments.ShowProductsFragment;
import com.trackitil.fragments.SignUpFragment;
import com.trackitil.logic.boundries.NewUserForm;
import com.trackitil.logic.entities.BarcodeMapEntity;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.Date;

public class AuthorizedUserActivity extends AppCompatActivity implements
        AddProductFragment.BarcodeScanListener,
        CalenderFragment.onDateListener,
        SignUpFragment.onSubmitButtonListener,
        GeneralDetailsFragment.CardClickListener,
        AlarmPickerFragment.onTimeListener,
        RemoveUserFragment.UserRemoveListener {

    /**
     * A firebaseAuth instance
     */
    private FirebaseAuth mAuth;

    /**
     * drawer layout to show menu
     */
    private DrawerLayout mDrawerLayout;

    /**
     * === Fragments of this activity ===
     */
    private AddProductFragment addProductFragment;
    private CalenderFragment calenderFragment;
    private SignUpFragment signUpFragment;
    private AlarmPickerFragment alarmPickerFragment;
    private RemoveUserFragment removeUserFragment;

    /**
     * Variables to store the role and branch from the MainActivity
     */
    private String role;
    private String branch;

    /**
     * The id of an icon to draw on the toolbar
     */
    private int toolBarIconDrawable;

    public static final String CHANNEL_ID = "1";
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        role = sharedPreferences.getString(ProjectUtils.ROLE_KEY, "");
        branch = sharedPreferences.getString(ProjectUtils.BRANCH_KEY, "");

        setContentView(R.layout.authorized_user);

        setToolBar(R.drawable.ic_menu);
        setNavigationMenu();
        initPrivateFields();
        goToHomeFragment();
        if (!sharedPreferences.getBoolean("FIRST_ALARM", false)) {
            setNotificationAlarm(7, 0);
        }
    }

    private void initPrivateFields() {
        mAuth = FirebaseAuth.getInstance();
        addProductFragment = new AddProductFragment();
        calenderFragment = new CalenderFragment();
        signUpFragment = new SignUpFragment();
        removeUserFragment = new RemoveUserFragment();
        alarmPickerFragment = new AlarmPickerFragment();
    }

    private void goToHomeFragment() {
        GeneralDetailsFragment generalDetailsFragment = new GeneralDetailsFragment();
        moveTo(generalDetailsFragment);
    }

    /**
     * Setting the toolbar
     *
     * @param res - the resource image to put on the toolbar
     */
    private void setToolBar(int res) {
        if (toolBarIconDrawable == res)
            return;
        setSupportActionBar(findViewById(R.id.toolbar));
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(res);
        }
        toolBarIconDrawable = res;
    }

    /**
     * Setting and configuring the menu
     */
    private void setNavigationMenu() {
        mDrawerLayout = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);

        if (role.equals(ProjectUtils.WORKER)) {
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.add_user).setVisible(false);
            nav_Menu.findItem(R.id.remove_user).setVisible(false);
        }

        navigationView.setNavigationItemSelectedListener(menuItem -> {
            mDrawerLayout.closeDrawers();
            switch (menuItem.getItemId()) {
                case R.id.show_inventory:
                    moveTo(new ShowProductsFragment());
                    navigationView.getMenu().getItem(0).setChecked(false);
                    break;
                case R.id.calender:
                    moveTo(calenderFragment);
                    navigationView.getMenu().getItem(0).setChecked(false);
                    break;
                case R.id.add_products:
                    moveTo(addProductFragment);
                    navigationView.getMenu().getItem(0).setChecked(false);
                    break;
                case R.id.logout:
                    onLogOut();
                    navigationView.getMenu().getItem(0).setChecked(false);
                    break;
                case R.id.add_user:
                    moveTo(signUpFragment);
                    navigationView.getMenu().getItem(0).setChecked(false);
                    break;
                case R.id.remove_user:
                    moveTo(removeUserFragment);
                    navigationView.getMenu().getItem(0).setChecked(false);
                    break;
                case R.id.alarm_picker:
                    moveTo(alarmPickerFragment);
                    navigationView.getMenu().getItem(0).setChecked(false);
                    break;
            }
            return true;
        });

        ((TextView) navigationView.getHeaderView(0).findViewById(R.id.headerUser))
                .setText(String.format("%s | %s", getIntent().getBundleExtra(ProjectUtils.BUNDLE_KEY).getString(ProjectUtils.PREF_NAME), branch));

        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
            if (getSupportFragmentManager().getBackStackEntryCount() > 1)
                setToolBar(R.drawable.ic_arrow_back);
            else {
                setToolBar(R.drawable.ic_menu);
                ((Toolbar) findViewById(R.id.toolbar)).setTitle(R.string.app_name);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (toolBarIconDrawable == R.drawable.ic_menu)
                    mDrawerLayout.openDrawer(GravityCompat.START);
                else
                    getSupportFragmentManager().popBackStackImmediate();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Move to different fragments
     *
     * @param fragment - the fragment to move to
     */
    public void moveTo(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    /**
     * Logs the user out
     */
    public void onLogOut() {
        mAuth.signOut();
        finish();
    }

    @Override
    public void onBarcodeScanner() {
        startActivity(new Intent(AuthorizedUserActivity.this, ScannerActivity.class));
    }

    @Override
    public void onAddBarcode() {
        TextInputEditText barcodeInput = findViewById(R.id.barcode_input);
        String barcode = barcodeInput.getText() != null ? barcodeInput.getText().toString() : "";
        if (!barcode.isEmpty()) {
            searchBarcodeInDatabase(barcode);
            ProjectUtils.clearFields(barcodeInput);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.no_manually_barcode);
            builder.setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss());
            builder.setCancelable(false);
            builder.show();
        }
    }

    @Override
    public void onDateClicked(String date) {
        ExpiredFragment expiredFragment = new ExpiredFragment();
        Bundle args = new Bundle();
        args.putString(ProjectUtils.DATE_KEY, date);
        expiredFragment.setArguments(args);
        moveTo(expiredFragment);
    }

    @Override
    public void onTimeClicked(String hour, String minute) {
        setNotificationAlarm(Integer.parseInt(hour), Integer.parseInt(minute));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        onLogOut();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1)
            return;
        super.onBackPressed();
    }

    @Override
    public void onSubmitClicked(NewUserForm userForm) {
        createAccount(userForm);
    }

    @Override
    public void onExpiredCardClicked() {
        onDateClicked(ProjectUtils.formatter.format(new Date()));
    }

    @Override
    public void onInventoryCardClicked() {
        moveTo(new ShowProductsFragment());
    }

    /**
     * Creates a new account, using the firebase authenticator
     *
     * @param userForm - the user form
     */
    private void createAccount(NewUserForm userForm) {
        if (ProjectUtils.validateForm(getString(R.string.field_error), userForm.getEditors())) {
            String userName = userForm.getUserNameValue();
            String email = userForm.getEmailValue();
            String password = userForm.getPasswordValue();
            mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    FirebaseUser currentUser = mAuth.getCurrentUser();
                    if (currentUser != null) {
                        // Adding the username to the users profile
                        currentUser.updateProfile(new UserProfileChangeRequest.Builder().setDisplayName(userName).build());
                        sendEmailVerification(currentUser);
                    }
                    FirebaseHelper.getInstance().addUser(userForm);
                } else {
                    ProjectUtils.makeToast(AuthorizedUserActivity.this, getString(R.string.failed_auth));
                }
                ProjectUtils.clearFields(userForm.getEditors());
            });
        }
    }

    /**
     * Sends a verification email
     *
     * @param user - the user to send the email to
     */
    private void sendEmailVerification(FirebaseUser user) {
        user.sendEmailVerification().addOnCompleteListener(task ->
                ProjectUtils.makeToast(AuthorizedUserActivity.this, getString(task.isSuccessful() ? R.string.verify_email : R.string.send_email_error)));
    }

    /**
     * Search the barcode specified in the database
     *
     * @param barcode - the barcode to search for
     */
    public void searchBarcodeInDatabase(String barcode) {
        // Query for product with the barcode above - short barcode
        Query shortBarcodeQuery = FirebaseHelper.getInstance().queryForShortBarcode(barcode);

        shortBarcodeQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // It is safe to assume that there's only one barcode, since the mapping is one-to-one
                if (dataSnapshot.exists()) {
                    BarcodeMapEntity bme = dataSnapshot.getChildren().iterator().next().getValue(BarcodeMapEntity.class);
                    if (bme != null) goToAddProductForm(bme);
                    shortBarcodeQuery.removeEventListener(this);
                } else {
                    // Query for product with the barcode above - long barcode
                    Query longBarcodeQuery = FirebaseHelper.getInstance().queryForLongBarcode(barcode);
                    longBarcodeQuery.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                BarcodeMapEntity bme = dataSnapshot.getChildren().iterator().next().getValue(BarcodeMapEntity.class);
                                if (bme != null) goToAddProductForm(bme);
                                longBarcodeQuery.removeEventListener(this);
                            } else {
                                barcodeNotInDatabase(barcode);
                                longBarcodeQuery.removeEventListener(this);
                                shortBarcodeQuery.removeEventListener(this);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    /**
     * Sends the user to the add product form
     *
     * @param bme - the barcode mapper to take the name and barcode from
     */
    private void goToAddProductForm(BarcodeMapEntity bme) {
        Bundle b = new Bundle();
        b.putString(ProjectUtils.BARCODE_KEY, bme.getBarcode());
        b.putString(ProjectUtils.PRODUCT_NAME_KEY, bme.getName());
        b.putString(ProjectUtils.BRANCH_KEY, branch);
        Intent intent = new Intent(AuthorizedUserActivity.this, AddProductFormActivity.class);
        intent.putExtra(ProjectUtils.BUNDLE_KEY, b);
        startActivity(intent);
    }

    /**
     * A prompt dialog to the user, stating that the barcode is not in the database
     *
     * @param barcode - the barcode the user tried to scan or add
     */
    public void barcodeNotInDatabase(String barcode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.no_barcode_in_database);

        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
            Bundle b = new Bundle();
            b.putString(ProjectUtils.BARCODE_KEY, barcode);
            Intent intent = new Intent(AuthorizedUserActivity.this, AddBarcodeMapperFormActivity.class);
            intent.putExtra(ProjectUtils.BUNDLE_KEY, b);
            startActivity(intent);
        });

        builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.dismiss());
        builder.setCancelable(false);
        builder.show();
    }

    private void setNotificationAlarm(int hour, int minute) {

        createNotificationChannel();

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);

        Calendar now = Calendar.getInstance();
        now.setTime(new Date());

        // If the time now is after the time we set for the alarm,
        // let the first time the alarm goes off be tomorrow
        if (now.after(calendar)) {
            calendar.add(Calendar.DATE, 1);
        }

        Intent intent1 = new Intent(this, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent1, 0);
        AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
        am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("FIRST_ALARM", true);
        editor.putString("HOUR", (hour < 10 ? "0" : "") + hour);
        editor.putString("MINUTE", (minute < 10 ? "0" : "") + minute);
        editor.commit();
    }

    /**
     * Creates a notification channel. This is essential to android versions Oreo and above. </br>
     * This function is ignored in case the device's android version is lower
     */
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, getString(R.string.expired_notifications), NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(getString(R.string.expired_notifications_descriptions));
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    public void onUserRemove(TextInputEditText emailField) {
        if (ProjectUtils.validateForm(getString(R.string.field_error), emailField)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(AuthorizedUserActivity.this);
            builder.setTitle(R.string.sureToRemove);
            builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
                FirebaseHelper.getInstance().removeUser(emailField.getText().toString());
                ProjectUtils.clearFields(emailField);
                ProjectUtils.makeToast(AuthorizedUserActivity.this, getString(R.string.successfulRemove));
            });
            builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.dismiss());
            builder.show();
        }
    }

    @Override
    public void onPieChartDateClicked(String date) {
        onDateClicked(date);
    }

    @Override
    public void onFloatingButtonClicked() {
        moveTo(addProductFragment);
    }
}