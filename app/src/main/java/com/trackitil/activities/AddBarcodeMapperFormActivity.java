package com.trackitil.activities;

import android.os.Bundle;

import com.trackitil.FirebaseHelper;
import com.trackitil.ProjectUtils;
import com.trackitil.R;
import com.google.android.material.textfield.TextInputEditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class AddBarcodeMapperFormActivity extends AppCompatActivity {
    TextInputEditText barcodeField;
    TextInputEditText nameField;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_barcode_mapper_form);
        String barcode = getIntent().getBundleExtra(ProjectUtils.BUNDLE_KEY).getString(ProjectUtils.BARCODE_KEY);

        barcodeField = findViewById(R.id.form_barcode_edit_text);
        nameField = findViewById(R.id.form_name_edit_text);
        barcodeField.setText(barcode);

        findViewById(R.id.createNewProduct).setOnClickListener(v -> handleForm());
    }

    /**
     * Handles the form upon clicking the create new product button
     */
    private void handleForm() {
        if (ProjectUtils.validateForm(getString(R.string.field_error), nameField)) {
            FirebaseHelper.getInstance().addBarcodeMapEntity(barcodeField.getText().toString(), nameField.getText().toString());
            ProjectUtils.clearFields(nameField);
            finish();
        }
    }
}