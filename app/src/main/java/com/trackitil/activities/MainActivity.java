package com.trackitil.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.trackitil.FirebaseHelper;
import com.trackitil.ProjectUtils;
import com.trackitil.R;
import com.trackitil.fragments.LoginFragment;
import com.trackitil.fragments.ResetPasswordFragment;

import com.trackitil.logic.entities.UserEntity;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

/**
 * TrackIt is an application that comes to serve the needs of the small (and big) convenience stores </br>
 * to be able to track their inventory and mainly the expiration dates of the products. </br>
 * </br>
 * The application uses the Firebase Real-Time Database to store it's users and products. </br>
 * Some of the features available in the app: </br>
 * 1. The ability to add products via the camera, by scanning barcodes </br>
 * 2. The ability to see the expiration dates of the products in the system, every day </br>
 * 3. A notification that pops up to the managers and workers of the store, indicating of expired products </br>
 * </br>
 * The application is secure and is closed to be able to be used by only the stores managers and workers
 * <p>
 * All right reserved to the authors below
 *
 * @author Ruby Kozel
 * @author Slava Kagan
 */
public class MainActivity extends AppCompatActivity implements
        LoginFragment.onButtonClickedListener,
        ResetPasswordFragment.ResetButtonListener {

    /**
     * A firebaseAuth instance
     */
    private FirebaseAuth mAuth;

    /**
     * Instance of SharedPreferences to store variables across the project
     */
    private SharedPreferences sharedPreferences;
    private FrameLayout progressBarFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();
        moveToLogin();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        progressBarFragment = findViewById(R.id.progress_bar_fragment);
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        String branchName = sharedPreferences.getString(ProjectUtils.BRANCH_KEY, "");
        String email = sharedPreferences.getString(ProjectUtils.EMAIL_KEY, "");
        if (branchName == null || email == null)
            return;
        if (currentUser != null && !branchName.equals("") && !email.equals("")) {
            progressBarFragment.setVisibility(View.VISIBLE);
            findViewById(R.id.fragment_container_login).setClickable(false);
            verifyUsersBranch(currentUser, email, branchName);
        }
    }

    @Override
    public void onLoginClicked(TextInputEditText mLoginEmailField, TextInputEditText mLoginPasswordField, String branchName) {
        signIn(mLoginEmailField, mLoginPasswordField, branchName);
    }

    @Override
    public void onForgotPasswordClicked() {
        moveToResetPassword();
    }

    @Override
    public void onResetClicked(TextInputEditText emailField) {
        resetPassword(emailField);
    }

    /**
     * Moves the user to the login fragment
     */
    private void moveToLogin() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container_login, new LoginFragment())
                .addToBackStack(null)
                .commit();
    }

    /**
     * Moves the user to the reset password fragment
     */
    public void moveToResetPassword() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container_login, new ResetPasswordFragment())
                .addToBackStack(null)
                .commit();
    }

    /**
     * Prompts the user a welcome message
     *
     * @param currentUser - the user to be greeted
     */
    private void promptUser(FirebaseUser currentUser) {
        if (currentUser != null)
            ProjectUtils.makeToast(MainActivity.this, getString(R.string.greeting, currentUser.getDisplayName()));
    }

    /**
     * Signs the user in, using Firebase Authentication
     *
     * @param emailField    - the email field
     * @param passwordField - the password field
     * @param branchName    - the branch this user is trying to log into
     */
    private void signIn(TextInputEditText emailField, TextInputEditText passwordField, String branchName) {
        if (emailField.getText() == null || passwordField.getText() == null)
            return;
        if (ProjectUtils.validateForm(getString(R.string.field_error), emailField, passwordField)) {
            progressBarFragment.setVisibility(View.VISIBLE);
            findViewById(R.id.fragment_container_login).setClickable(false);
            String email = emailField.getText().toString();
            String password = passwordField.getText().toString();
            ProjectUtils.clearFields(emailField, passwordField);
            mAuth.signInWithEmailAndPassword(email.toLowerCase(), password).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    FirebaseUser user = mAuth.getCurrentUser();
                    if (user != null && user.isEmailVerified()) {
                        verifyUsersBranch(user, email, branchName);
                    } else {
                        progressBarFragment.setVisibility(View.GONE);
                        findViewById(R.id.fragment_container_login).setClickable(true);
                        ProjectUtils.makeToast(MainActivity.this, getString(R.string.verify_first));
                    }
                } else {
                    progressBarFragment.setVisibility(View.GONE);
                    findViewById(R.id.fragment_container_login).setClickable(true);
                    ProjectUtils.makeToast(MainActivity.this, getString(R.string.failed_auth));
                }
            });
        }
    }

    private void verifyUsersBranch(FirebaseUser user, String email, String branchName) {
        /* Querying the user with the email specified,
           checking if he belongs to the branch he specified */
        Query userQuery = FirebaseHelper.getInstance().getUser(email);
        userQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    if (!checkUsersBranchAuth(dataSnapshot, user, email, branchName))
                        ProjectUtils.makeToast(MainActivity.this, getString(R.string.failed_auth));
                } else {
                    ProjectUtils.makeToast(MainActivity.this, getString(R.string.mailNotExist, email));
                }
                progressBarFragment.setVisibility(View.GONE);
                findViewById(R.id.fragment_container_login).setClickable(true);
                userQuery.removeEventListener(this);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    /**
     * This method sends a reset password email to the email specified
     *
     * @param emailField - the email field from which the method takes the email address to send the email
     */
    private void resetPassword(TextInputEditText emailField) {
        if (ProjectUtils.validateForm(getString(R.string.field_error), emailField)) {
            String email = emailField.getText() != null ? emailField.getText().toString() : "";
            ProjectUtils.clearFields(emailField);
            mAuth.sendPasswordResetEmail(email).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    ProjectUtils.makeToast(MainActivity.this, getString(R.string.mailPassowordSent, email));
                    getSupportFragmentManager().popBackStackImmediate();
                } else {
                    ProjectUtils.makeToast(MainActivity.this, getString(R.string.mailNotExist, email));
                }
            });
        }
    }

    /**
     * Checks if the user is trying to access his own branch
     *
     * @param dataSnapshot - the dataSnapshot from the query
     * @param user         - the user to check
     * @param branchName   - the branch name
     * @return - false if the user is trying to access a branch he doesn't belong to. </br>
     * Note: the method moves the user to the authorized section if everything is valid, and returns true
     */
    private boolean checkUsersBranchAuth(@NotNull DataSnapshot dataSnapshot, FirebaseUser user, String email, String branchName) {
        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            UserEntity userEntity = ds.getValue(UserEntity.class);
            if (userEntity == null)
                return false;
            if (userEntity.getBranchName().equals(branchName) && userEntity.getName().equals(user.getDisplayName())) {
                saveUsersInfo(branchName, email, userEntity.getRole());
                promptUser(user);
                goToAuthorizedSection(user);
                return true;
            }
        }
        return false;
    }

    /**
     * Saves the branch and role to the shared preferences
     *
     * @param branchName - the branch name to save
     * @param role       - the role to save
     */
    private void saveUsersInfo(String branchName, String email, String role) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ProjectUtils.BRANCH_KEY, branchName);
        editor.putString(ProjectUtils.ROLE_KEY, role);
        editor.putString(ProjectUtils.EMAIL_KEY, email);
        editor.commit(); // Using commit to make the saving be immediate
    }

    /**
     * Sending the user to the authorized section
     *
     * @param user - the user to send
     */
    private void goToAuthorizedSection(FirebaseUser user) {
        promptUser(user);
        Bundle b = new Bundle();
        b.putString(ProjectUtils.EMAIL_KEY, user.getEmail());
        b.putString(ProjectUtils.PREF_NAME, user.getDisplayName());
        Intent intent = new Intent(MainActivity.this, AuthorizedUserActivity.class);
        intent.putExtra(ProjectUtils.BUNDLE_KEY, b);
        startActivity(intent);
    }


    /**
     * Sending user back
     */

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
            return;
        }
        super.onBackPressed();
    }
}