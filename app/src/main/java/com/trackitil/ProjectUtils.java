package com.trackitil;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * An utility class that contains the constants and the utility methods that are used across the project
 */
public class ProjectUtils {

    /**
     * The formatter of the date
     */
    public static final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

    /* ==== CONSTANTS ==== */
    public static final String BRANCH_KEY = "BRANCH_KEY";
    public static final String BUNDLE_KEY = "BUNDLE_KEY";
    public static final String DATE_KEY = "DATE_KEY";
    public static final String BARCODE_KEY = "BARCODE_KEY";
    public static final String PRODUCT_NAME_KEY = "PRODUCT_NAME_KEY";
    public static final String EMAIL_KEY = "EMAIL_KEY";
    public static final String ROLE_KEY = "ROLE_KEY";
    public static final String PREF_NAME = "PREF_NAME";
    public static final String WORKER = "worker";
    public static final String MANAGER = "manager";

    private ProjectUtils() {
    }

    /**
     * Validates that the <b>editTexts</b> provided are valid
     *
     * @param error     - the error message to put in the editTexts in case they are not valid
     * @param editTexts - the edit texts to check
     * @return - true if everything is valid, false if even one of the editTexts is invalid
     */
    public static boolean validateForm(String error, TextInputEditText... editTexts) {
        for (TextInputEditText editText : editTexts) {
            if (TextUtils.isEmpty(editText.getText().toString())) {
                editText.setError(error);
                return false;
            } else
                editText.setError(null);
        }
        return true;
    }

    /**
     * Validates that the <b>editTexts</b> provided are valid
     *
     * @param error     - the error message to put in the editTexts in case they are not valid
     * @param editTexts - the edit texts to check
     * @return - true if everything is valid, false if even one of the editTexts is invalid
     */
    public static boolean validateForm(String error, List<TextInputEditText> editTexts) {
        for (TextInputEditText editText : editTexts) {
            if (TextUtils.isEmpty(editText.getText().toString())) {
                editText.setError(error);
                return false;
            } else
                editText.setError(null);
        }
        return true;
    }

    /**
     *
     * @param editTexts- the edit texts to clear
     */
    public static void clearFields(TextInputEditText ... editTexts) {
        for(TextInputEditText editText : editTexts) {
            editText.setText("");
        }
    }

    /**
     *
     * @param editTexts- the edit texts to clear
     */
    public static void clearFields(List<TextInputEditText> editTexts) {
        for(TextInputEditText editText : editTexts) {
            editText.setText("");
        }
    }

    /**
     * Returns the date in a dd/MM/yyyy format
     *
     * @param dayOfMonth - the day (dd)
     * @param month      - the month (MM)
     * @param year       - the year (yyyy)
     * @return - the date in the format specified
     */
    public static String getDate(int dayOfMonth, int month, int year) {
        return getDate(dayOfMonth + "/" + month + "/" + year);
    }

    /**
     * Validates the date string with the format dd/MM/yyyy
     *
     * @param date - the date to be validates
     * @return - true if the date is valid, false otherwise
     */
    public static boolean validateDate(String date) {
        try {
            formatter.parse(date);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * Returns the date in a dd/MM/yyyy format
     *
     * @param date - the date in the structure of X/X/X
     * @return - the formatted date. Note: null might be returned if the date passed is not in the valid format
     */
    public static String getDate(String date) {
        try {
            return ProjectUtils.formatter.format(ProjectUtils.formatter.parse(date));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String addToDate(String date, int i) {
        try {
            final Date dt = formatter.parse(date);
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(dt);
            calendar.add(Calendar.DAY_OF_YEAR, i);
            return formatter.format(calendar.getTime());
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static void makeToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }
}