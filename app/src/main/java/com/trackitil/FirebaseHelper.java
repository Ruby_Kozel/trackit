package com.trackitil;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.trackitil.logic.boundries.NewProductForm;
import com.trackitil.logic.boundries.NewUserForm;
import com.trackitil.logic.entities.BarcodeMapEntity;
import com.trackitil.logic.entities.ProductEntity;
import com.trackitil.logic.entities.UserEntity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;

import java.util.List;

import androidx.annotation.NonNull;

public class FirebaseHelper implements DatabaseHelper {

    private static FirebaseHelper instance;

    /* ==== CONSTANTS ==== */
    private static final String PRODUCTS = "products";
    private static final String BARCODE_MAPPERS = "barcode_mappers";
    private static final String USERS = "users";
    private static final String EXPIRATION_DATE = "expirationDate";
    private static final String BARCODE = "barcode";
    private static final String EMAIL = "email";

    private FirebaseDatabase fireBaseDatabase;

    /**
     * A helper class for fetching and storing data in and from the firebase real-time database
     */
    private FirebaseHelper() {
        fireBaseDatabase = FirebaseDatabase.getInstance();
    }

    public static FirebaseHelper getInstance() {
        if (instance == null)
            instance = new FirebaseHelper();
        return instance;
    }

    /**
     * Adds a new barcode mapper to the database
     *
     * @param barcode - the barcode to be mapped
     * @param name    - the name that the barcode is going to be mapped to
     */
    @Override
    public void addBarcodeMapEntity(String barcode, String name) {
        DatabaseReference mFireBaseDatabase = fireBaseDatabase.getReference(BARCODE_MAPPERS);
        String uniqueKey = mFireBaseDatabase.push().getKey();
        if (uniqueKey != null)
            mFireBaseDatabase.child(uniqueKey).setValue(new BarcodeMapEntity(barcode, name));
    }

    /**
     * Adds a new product to the database
     *
     * @param productForm - the products form
     */
    @Override
    public void addProduct(NewProductForm productForm) {
        DatabaseReference mFireBaseDatabase = fireBaseDatabase.getReference(PRODUCTS);
        String uniqueKey = mFireBaseDatabase.push().getKey();
        if (uniqueKey != null)
            mFireBaseDatabase.child(uniqueKey)
                    .setValue(new ProductEntity(uniqueKey, productForm));
    }

    /**
     * Adds a new user to the database
     *
     * @param userForm - the user form
     */
    @Override
    public void addUser(NewUserForm userForm) {
        Log.d("NEW USER FORM*****", userForm.toString());
        DatabaseReference mFireBaseDatabase = fireBaseDatabase.getReference(USERS);
        String uniqueKey = mFireBaseDatabase.push().getKey();
        if (uniqueKey != null)
            mFireBaseDatabase.child(uniqueKey).setValue(new UserEntity(userForm));
    }

    public void removeUser(String email) {
        FirebaseDatabase
                .getInstance()
                .getReference(USERS)
                .orderByChild(EMAIL)
                .equalTo(email)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            String key = dataSnapshot.getChildren().iterator().next().getKey();
                            if (key != null)
                                FirebaseDatabase
                                        .getInstance()
                                        .getReference(USERS)
                                        .child(key)
                                        .removeValue();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    /**
     * @return - A Query that queries over all the products in the database, ordered by expiration date
     */
    @Override
    public Query getAllProducts() {
        return FirebaseDatabase
                .getInstance()
                .getReference(PRODUCTS)
                .orderByChild(EXPIRATION_DATE);
    }

    /**
     * Gets a query on a specific user
     *
     * @param email - the email of the user
     * @return - the query noted above
     */
    @Override
    public Query getUser(String email) {
        return FirebaseDatabase
                .getInstance()
                .getReference(USERS)
                .orderByChild(EMAIL)
                .equalTo(email);
    }

    /**
     * Given an id, this method removes the product with the specified id
     *
     * @param id - the id of the product to remove
     */
    @Override
    public void removeProductById(String id) {
        getProductById(id).removeValue();
    }

    @Override
    public DatabaseReference getProductById(String id) {
        return FirebaseDatabase
                .getInstance()
                .getReference(PRODUCTS)
                .child(id);
    }

    /**
     * Gets a Query that queries over the products with a specific expiration date
     *
     * @param date       - the expiration date
     * @return - the Query noted above
     */
    @Override
    public Query getExpirationEqualTo(String date) {
       return FirebaseDatabase
                .getInstance()
                .getReference(PRODUCTS)
                .orderByChild(EXPIRATION_DATE)
                .equalTo(date);
    }

    /**
     * An Israeli barcode often starts with a prefix of 7290*, for example </br>
     * 729000146789 </br>
     * Some of the barcodes are known by their short version, which means taking off the prefix </br>
     * So the barcode above can be also known as </br>
     * 146789 </br>
     * This methods takes off the prefix of the barcode (if it exists) and checks the database if the product with the short barcode exists
     *
     * @param barcode - the barcode to check
     * @return - A Query that queries the specific barcode mapper
     */
    @Override
    public Query queryForShortBarcode(String barcode) {
        barcode = barcode.replaceFirst("729[0]*", "");
        return FirebaseDatabase
                .getInstance()
                .getReference(BARCODE_MAPPERS)
                .orderByChild(BARCODE)
                .equalTo(barcode);
    }

    /**
     * Same as {@link #queryForShortBarcode(String) queryForShortBarcode}, only it does not take off the prefix
     *
     * @param barcode - the barcode to check
     * @return - A Query that queries the specific barcode mapper
     */
    @Override
    public Query queryForLongBarcode(String barcode) {
        return FirebaseDatabase
                .getInstance()
                .getReference(BARCODE_MAPPERS)
                .orderByChild(BARCODE)
                .equalTo(barcode);
    }

    /**
     * This method tries to detect a barcode from an image specified
     *
     * @param image             - the image to try to recognize the barcode from
     * @param onSuccessListener - the success listener, in case the recognition worked
     * @param onFailureListener - the failure listener, in case the recognition didn't work
     */
    public void detectBarcodesFrom(
            FirebaseVisionImage image,
            OnSuccessListener<? super List<FirebaseVisionBarcode>> onSuccessListener,
            OnFailureListener onFailureListener) {
        FirebaseVision
                .getInstance()
                .getVisionBarcodeDetector()
                .detectInImage(image)
                .addOnSuccessListener(onSuccessListener)
                .addOnFailureListener(onFailureListener);
    }
}